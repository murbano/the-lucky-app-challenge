FROM node:16.13.0-alpine

RUN npm i npm@latest -g

RUN mkdir /app && chown -R node:node /app
WORKDIR /app

# least privilege
USER node
COPY --chown=node:node package.json package-lock*.json ./
RUN npm install && npm cache clean --force

COPY --chown=node:node . .

CMD npm run start:dev


# For better version follow the Bret Fisher presentation at Docker Con 19 
# https://www.youtube.com/watch?v=yeZqoh3-cME
# https://www.bretfisher.com/docker/
