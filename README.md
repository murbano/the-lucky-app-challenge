# The Lucky APP Backend NestJS code challenge

## 1. Getting started

### 1.1 Requirements

Before starting, make sure you have those tools installed:

- [Docker](https://www.docker.com/)
- [NodeJS](https://nodejs.org/)
- [npm](https://www.npmjs.com/)

### **Disclaimer**

I know that .env never has to be pushed to the repo and I added it in order to avoid you an unnecessary step.

## 1.2 Project configuration & launching

This project uses Docker for a painless and smooth initialization of the challenge.

- Clone it from `git clone git@bitbucket.org:murbano/the-lucky-app-challenge.git`
- Install dependecies project `npm i`
- Finally run `docker compose up` to start API, Postgres and Redis.
- REST API should start at port 1337

## Test

### Postman Collection

I added a Postman Collecton to simplify testing, download it from [here](https://bitbucket.org/murbano/the-lucky-app-challenge/src/master/The%20Lucky%20App%20REST%20API.postman_collection.json)

### CURLs

If you go for an old school way, you can test the REST API by copying the following curls

#### User creation

```
curl -X POST \
  http://localhost:1337/api/v1/users \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"name": "Diego",
	"username": "diego.maradona",
	"street": "Segurola y Havana",
	"cityId": 1,
	"password": "laM4n0d3d!05"
}'
```

#### User authentication

```
curl -X POST \
  http://localhost:1337/api/v1/auth/login \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"username": "diego.maradona",
	"password": "laM4n0d3d!05"
}'
```

#### User profile

```
curl -X GET \
  http://localhost:1337/api/v1/users/profile \
  -H 'authorization: Bearer  THE JWT TOKEN FROM PREVIOUS API CALL' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
}'
```

## Assessment:

### Requirements (3 days):

Use NestJs to create a restful-API using the db scheme and the endpoint description in the AppIndex

- Try not to use the ORM for serialization and making the query.

- Use custom validation pipe /decorators

- Document how to fire up the project

- Please use a git based version control and share the repo with us

### Extra (2 days):

- Use redis or any other in-memory cache technique.

- Create docker containers for the user service with it's database. The system consists of three containers:
  A relational database service of your choice (ex: mysql, mssql, postgres, etc...)

- A node js service running the NestJs framework

- A redis instance

### Extra for a full stack position (2 days):

- Create react app (looks are not important) using the endpoints you created in NestJs and ship it with the project

- Login / Register / Profile

## AppIndex:

- Database: The database structure should be the following:

### User table:

| Id       | primary key |
| -------- | ----------- |
| username |
| password |

Profile table:

| id        | primary key           |
| --------- | --------------------- |
| userId    | forgien key (User)    |
| addressId | forgien key (Address) |
| name      |

Address table:

| id     | primary key        |
| ------ | ------------------ |
| cityId | forgien key (City) |
| street |

City table:
id | primary key
--- | ---
countryId |
name |

Country table:

| id   | primary key |
| ---- | ----------- |
| name |

Feel free to initialize the city and country tables for simplicity.

### REST-API:

#### The API's endpoints:

| Method   | Description                                                                                               |
| -------- | --------------------------------------------------------------------------------------------------------- |
| **POST** | Creates a user given (username,password,name,anddres,cityId)                                              |
| **POST** | returns a valid JWT token given (username,password)                                                       |
| **GET**  | Return a relevant user profile given a valid JWT token in a Authorization header with following structure |

```
{
  "id":1,
  "name":"Juan Luis",
  "address":{
    "street": "Dam square",
    "city":"Amsterdam",
    "country":"Netherlands"
   }
 }
```
