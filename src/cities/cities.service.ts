import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CityEntity } from './city.entity';
import { Cache } from 'cache-manager';

@Injectable()
export class CitiesService {
  private readonly logger = new Logger(CitiesService.name);

  constructor(
    @InjectRepository(CityEntity)
    private cityRepository: Repository<CityEntity>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) { }

  async getFromCache(): Promise<any> {
    // get city from redis cache
    let cities = await this.cacheManager.get('cities');

    if (!cities) {
      this.logger.log('Cities cache is empty.');
      cities = await this.cityRepository.find();
      this.cacheManager.set('cities', cities);
      this.logger.log('Cities cache set.');
    }

    return cities;
  }

  async findOne(id: number): Promise<CityEntity> {
    const cities = await this.getFromCache();

    const [city] = cities.filter((c) => id === c.id);

    return city || null;
  }
}
