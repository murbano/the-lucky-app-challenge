import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('cities')
export class CityEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  countryId: number;

  @Column({ unique: true })
  name: string;
}
