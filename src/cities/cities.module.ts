import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CityEntity } from './city.entity';
import { CitiesService } from './cities.service';

@Module({
  imports: [TypeOrmModule.forFeature([CityEntity])],
  providers: [CitiesService],
  controllers: [],
})
export class CityModule {}
