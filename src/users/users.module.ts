import { CityEntity } from './../cities/city.entity';
import { CitiesService } from 'src/cities/cities.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserEntity } from './user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, CityEntity])],
  controllers: [UsersController],
  providers: [UsersService, CitiesService],
  exports: [UsersService],
})
export class UsersModule { }
