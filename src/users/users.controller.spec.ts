// import { DatabaseModule } from './../database/database.module';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { CitiesService } from '../cities/cities.service';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDto } from './dtos/create.dto';
import { BadRequestException } from '@nestjs/common';

const createFakeDTO = () => {
  const dto = new CreateUserDto();
  dto.name = 'manolo';

  return dto;
};

const cityMock = { id: 1, name: 'Capital Federal', countryId: 10 };

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: UsersService;
  let cityService: CitiesService;

  const mockConnection = () => ({
    createQueryRunner: jest.fn(),
    connect: jest.fn(),
    startTransaction: jest.fn(),
    commitTransaction: jest.fn(),
    rollbackTransaction: jest.fn(),
    release: jest.fn(),
  });

  jest.mock('typeorm', () => ({
    Connection: () => mockConnection,
  }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            create: jest.fn(),
            findOneByUsername: jest.fn(),
          },
        },
        {
          provide: CitiesService,
          useValue: {
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    usersService = module.get<UsersService>(UsersService);
    cityService = module.get<CitiesService>(CitiesService);
  });

  it('should be defined all services', () => {
    expect(controller).toBeDefined();
    expect(usersService).toBeDefined();
    expect(cityService).toBeDefined();
  });

  describe('create', () => {
    it('should fail if city is invalid', async () => {
      jest
        .spyOn(cityService, 'findOne')
        .mockImplementation(() => Promise.resolve(null));

      const user = createFakeDTO();

      // await expect(controller.signup(user)).rejects.toThrow('Invalid city');
      try {
        await controller.signup(user);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('Invalid city');
        expect(error.response.statusCode).toBe(400);
        expect(error.response.error).toBe('Bad Request');
        expect(error.status).toBe(400);
      }
    });

    it('should fail if create() fails', async () => {
      jest
        .spyOn(cityService, 'findOne')
        .mockImplementation(() => Promise.resolve(cityMock));

      jest
        .spyOn(usersService, 'create')
        .mockImplementation(() => Promise.reject(new Error('some error')));

      const user = createFakeDTO();

      try {
        await controller.signup(user);
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe('some error');
      }
    });

    it('should fail if username has been created before', async () => {
      jest
        .spyOn(cityService, 'findOne')
        .mockImplementation(() => Promise.resolve(cityMock));

      jest.spyOn(usersService, 'create').mockImplementation(() =>
        Promise.reject({
          // new QueryFailedError(
          //   'INSERT INTO "users"("username", "password") VALUES ($1, $2) RETURNING "id"',
          //   [
          //     'diego.maradona',
          //     '$2a$12$HyyYdfrwhWskoq.XWQQs7OkkGTMI6f7BN94GrTo6borgD0aPU4b3a',
          //   ],
          //   'duplicate key value violates unique constraint "users_username_key',
          // ),
          code: '23505', // error code returned by TypeORM
        }),
      );

      const user = createFakeDTO();

      try {
        await controller.signup(user);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error.message).toBe('Username invalid, please choose another.');
        expect(error.response.statusCode).toBe(400);
        expect(error.response.error).toBe('Bad Request');
        expect(error.status).toBe(400);
      }
    });

    it('should create user as expected', async () => {
      jest
        .spyOn(cityService, 'findOne')
        .mockImplementation(() => Promise.resolve(cityMock));

      jest
        .spyOn(usersService, 'create')
        .mockImplementation(() => Promise.resolve({ id: 10 }));

      const user = createFakeDTO();

      await expect(controller.signup(user)).resolves.toEqual({ id: 10 });
    });
  });
});
