import * as bcrypt from 'bcryptjs';
import { AddressEntity } from './../addresses/address.entity';
import { Injectable, Logger } from '@nestjs/common';
import { ProfileEntity } from '../profiles/profile.entity';
import { Connection, getManager } from 'typeorm';
import { UserEntity } from './user.entity';
import { CreateUserDto } from './dtos/create.dto';

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);

  constructor(private connection: Connection) { }

  async create(options: CreateUserDto): Promise<any> {
    const { username, cityId, password, street, name } = options;
    const hashedPassword = await bcrypt.hash(password, 12);

    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    let user, profile, address;
    try {
      user = await queryRunner.manager.save(UserEntity, {
        username,
        password: hashedPassword,
      });

      address = await queryRunner.manager.save(AddressEntity, {
        cityId,
        street,
      });

      profile = await queryRunner.manager.save(ProfileEntity, {
        name,
        addressId: address.id,
        userId: user.id,
      });
      await queryRunner.commitTransaction();
    } catch (e) {
      this.logger.error('Error creating user', e);
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }

    this.logger.log('User', user);
    this.logger.log('Address', address);
    this.logger.log('Profile', profile);

    return {
      id: user.id,
    };
  }

  async findOneByUsername(username: string): Promise<any> {
    const entityManager = getManager();

    const result = await entityManager.query(
      `SELECT u.id, u.password, p.name, a.street, c.name AS city, co.name AS country FROM users u
      INNER JOIN profiles p ON u.id = p."userId"
      INNER JOIN addresses a ON a.id = p."addressId"
      INNER JOIN cities c ON c.id = a."cityId"
      INNER JOIN countries co ON co.id = c."countryId"
      WHERE u.username=$1`,
      [username],
    );

    if (!result.length) {
      return null;
    }

    const { id, password, name, street, city, country } = result.pop();

    return {
      id,
      password,
      name,
      address: {
        street,
        city,
        country,
      },
    };
  }
}
