import {
  BadRequestException,
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { PG_UNIQUE_CONSTRAINT_VIOLATION } from '../shared/database-error-codes';
import { JwtAuthGuard } from '../auth/jwt-auth.guards';
import { CreateUserDto } from './dtos/create.dto';
import { CitiesService } from '../cities/cities.service';

@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {
  constructor(
    private usersService: UsersService,
    private cityService: CitiesService) { }

  @HttpCode(201)
  @Post()
  async signup(@Body(new ValidationPipe()) userDto: CreateUserDto) {
    try {
      const city = await this.cityService.findOne(userDto.cityId);
      if (!city) {
        throw new BadRequestException('Invalid city');
      }

      const { id } = await this.usersService.create(userDto);
      return { id };
    } catch (error) {
      console.log(error);
      if (error.code === PG_UNIQUE_CONSTRAINT_VIOLATION) {
        throw new BadRequestException(
          'Username invalid, please choose another.',
        );
      }

      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
