export interface AddressInterface {
  street: string;
  city: string;
  country: string;
}
