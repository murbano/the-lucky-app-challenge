import { AddressInterface } from './../addresses/address.interface';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { UsersService } from 'src/users/users.service';

interface JwtUserInterface {
  id: number;
  name: string;
  address: AddressInterface;
}
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validate(username: string, password: string): Promise<any> {
    const user = await this.usersService.findOneByUsername(username);

    if (user && (await bcrypt.compare(password, user.password))) {
      delete user.password;

      return user;
    }

    return null;
  }

  async login(user: JwtUserInterface) {
    const payload = { sub: user.id, ...user };
    delete payload.id; // already sent in sub

    // sign JWT with user information
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
