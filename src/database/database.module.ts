import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DATABASE_HOST'),
        port: configService.get('DATABASE_PORT'),
        username: configService.get('DATABASE_USER'),
        password: configService.get('DATABASE_PASSWORD'),
        database: configService.get('DATABASE_NAME'),

        entities: ['dist/**/*.entity{.ts,.js}'],
        autoLoadEntities: true,

        synchronize: false,

        migrationsRun: true, // run always on start
        migrationsTableName: 'migrations',
        migrations: ['dist/database/migrations/*{.ts,.js}'],
      }),
    }),
  ],
})
export class DatabaseModule {}
