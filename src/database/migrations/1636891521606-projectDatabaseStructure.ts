import { Logger } from '@nestjs/common';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class projectDatabaseStructure1636891521606
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    Logger.log(`Executing migration with database structure`);
    await queryRunner.query(`
      -- Create tables
      DROP TABLE IF EXISTS addresses;
      CREATE TABLE addresses (
        id SERIAL PRIMARY KEY,
        "cityId" NUMERIC NOT NULL,
        street VARCHAR NOT NULL
      );

      DROP TABLE IF EXISTS cities;
      CREATE TABLE cities (
        id SERIAL PRIMARY KEY,
        "countryId" NUMERIC NOT NULL,
        name VARCHAR NOT NULL
      );

      DROP TABLE IF EXISTS countries;
      CREATE TABLE countries (
        id SERIAL PRIMARY KEY,
        name VARCHAR NOT NULL
      );

      DROP TABLE IF EXISTS profiles;
      CREATE TABLE profiles (
        id SERIAL PRIMARY KEY,
        "userId" NUMERIC NOT NULL,
        "addressId" NUMERIC NOT NULL,	
        name VARCHAR NOT NULL
      );

      DROP TABLE IF EXISTS users;
      CREATE TABLE users (
        id SERIAL PRIMARY KEY,
        username VARCHAR UNIQUE NOT NULL,
        password VARCHAR NOT NULL
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    Logger.warn(`Rolling back migration with database structure`);
    await queryRunner.query(`DROP TABLE IF EXISTS addresses;
      DROP TABLE IF EXISTS cities;
      DROP TABLE IF EXISTS countries;
      DROP TABLE IF EXISTS profiles;
      DROP TABLE IF EXISTS users;`);
  }
}
