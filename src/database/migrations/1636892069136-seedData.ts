import { Logger } from '@nestjs/common';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class seedData1636892069136 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    Logger.log(`Executing migration with seed data`);
    await queryRunner.query(
      `INSERT INTO countries (name) VALUES ('Argentina'), ('Spain');`,
    );
    await queryRunner.query(
      `INSERT INTO cities ("countryId", name) VALUES (1, 'Buenos Aires'), (1, 'Mar del Plata'), (2, 'Barcelona'), (2, 'Madrid');`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    Logger.warn(`Rolling back migration with seed data`);
    await queryRunner.query(`
      TRUNCATE TABLE countries;
      TRUNCATE TABLE cities;
    `);
  }
}
